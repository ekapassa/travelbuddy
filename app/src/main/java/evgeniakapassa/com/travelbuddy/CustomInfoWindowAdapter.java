package evgeniakapassa.com.travelbuddy;

import android.app.Activity;
import android.media.Rating;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter{

    private Activity context;

    public CustomInfoWindowAdapter(Activity context){
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = context.getLayoutInflater().inflate(R.layout.customwindow, null);
        String snippet = marker.getSnippet();
        String lines[] = snippet.split("\\r?\\n");

        String placeid = lines[0];
        String placeRating = lines[1];

        TextView placeName = (TextView) view.findViewById(R.id.place_name);
        TextView placeId = (TextView) view.findViewById(R.id.place_id);
        TextView pLlaceRating = (TextView) view.findViewById(R.id.place_rating);
        RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);

        placeName.setText(marker.getTitle());
        placeId.setText(placeid);
        pLlaceRating.setText(placeRating);

        Float rate = Float.valueOf(placeRating);
        int rateRound = rate.intValue();
        ratingBar.setRating(rateRound);
        return view;
    }

}
