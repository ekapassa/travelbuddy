package evgeniakapassa.com.travelbuddy;

public class AvailableCity {

    private String city_name;
    private int city_img_id;

    public AvailableCity(String city_name, int city_img_id) {

        this.city_name = city_name;
        this.city_img_id = city_img_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public int getCity_img_id() {
        return city_img_id;
    }

    public void setCity_img_id(int city_img_id) {
        this.city_img_id = city_img_id;
    }

}
