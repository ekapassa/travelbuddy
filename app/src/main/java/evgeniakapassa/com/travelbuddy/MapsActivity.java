package evgeniakapassa.com.travelbuddy;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;

import evgeniakapassa.com.travelbuddy.POJO.Place;
import evgeniakapassa.com.travelbuddy.retrofit.PlacesApi;
import evgeniakapassa.com.travelbuddy.utils.G;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private double lat;
    private double lng;
    private  String placeid;
    int PROXIMITY_RADIUS = 5000;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_cafes:
                    build_retrofit_and_get_response("cafe");
                    return true;
                case R.id.navigation_restaurants:
                    build_retrofit_and_get_response("restaurant");
                    return true;
                case R.id.navigation_bars:
                    build_retrofit_and_get_response("bar");
                    return true;
                case R.id.navigation_shopping:
                    build_retrofit_and_get_response("clothing_store");
                    return true;
                case R.id.navigation_sightseeing:
                    build_retrofit_and_get_response("museum");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // getIntent() is a method from the started activity
        Intent intent = getIntent(); // gets the previously created intent
        lat = getIntent().getDoubleExtra("lat", lat);
        lng = getIntent().getDoubleExtra("lng",lng);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //initialize Bottom Navigation
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        // Add a marker in selected city and move the camera
        float zoom = 15;
        LatLng selectedCity = new LatLng(lat, lng) ;

        int height = 250;
        int width = 150;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.my_marker);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.my_marker);
        mMap.addMarker(new MarkerOptions().position(selectedCity).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedCity, zoom));
    }



    private void build_retrofit_and_get_response(String type) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(G.GOOGLE_PLACES_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PlacesApi service = retrofit.create(PlacesApi.class);
        Call<Place> call = service.getNearbyPlaces(type, lat + "," + lng, PROXIMITY_RADIUS);
        call.enqueue(new Callback<Place>(){
            @Override
            public void onResponse(final Response<Place> response, Retrofit retrofit) {
                try {
                    mMap.clear();
                    // This loop will go through all the results and add marker on each location.
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        final Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                        final Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLng();

                        final String placeName = response.body().getResults().get(i).getName();
                        placeid = response.body().getResults().get(i).getPlaceId();
                        final Double placeRating = response.body().getResults().get(i).getRating();

                        final MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);
                        markerOptions.position(latLng);
                        markerOptions.title(placeName);
                        markerOptions.snippet(placeid+"\n"+placeRating);
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
                        //Set Custom InfoWindow Adapter
                        CustomInfoWindowAdapter adapter = new CustomInfoWindowAdapter(MapsActivity.this);
                        mMap.setInfoWindowAdapter(adapter);

                        mMap.addMarker(markerOptions);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                mMap.addMarker(markerOptions).showInfoWindow();
                                return false;
                            }
                        });
                        // Set a listener for info window events.
                        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                            @Override
                            public void onInfoWindowClick(Marker marker) {

                                String snippet = marker.getSnippet();
                                String lines[] = snippet.split("\\r?\\n");

                                String placeid = lines[0];
                                String placeRating = lines[1];

                                //push stin array 0--> placeid kai 1-->placeName
                                String array[] = {placeid,marker.getTitle()};


                                Intent intent = new Intent(MapsActivity.this, PlaceDetailsActivity.class);
                                intent.putExtra("extras", array);
                                startActivity(intent);
                            }
                        });
                    }
                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent_profile = new Intent(MapsActivity.this, UserProfileActivity.class);
                startActivity(intent_profile);
                return true;
            case R.id.favPlaces:
                Intent intent_fav = new Intent(MapsActivity.this, FavoritePlaceActivity.class);
                startActivity(intent_fav);
                return true;
            case R.id.signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MapsActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
