package evgeniakapassa.com.travelbuddy.retrofit;

import evgeniakapassa.com.travelbuddy.POJO.Place;
import evgeniakapassa.com.travelbuddy.POJO.PlaceDetails;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface PlacesApi {

    @GET ("api/place/nearbysearch/json?sensor=true&key=AIzaSyBHH-evj6wE0w0c5iqYqkajntJQdodmevM")
    Call<Place> getNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);

    @GET ("api/place/details/json?key=AIzaSyBHH-evj6wE0w0c5iqYqkajntJQdodmevM")
    Call<PlaceDetails> getPlaceDetails(@Query("placeid") String placeid);
}
