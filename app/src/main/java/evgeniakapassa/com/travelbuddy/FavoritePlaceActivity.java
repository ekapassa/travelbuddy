package evgeniakapassa.com.travelbuddy;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import evgeniakapassa.com.travelbuddy.POJO.User;

import static android.widget.Toast.LENGTH_LONG;

public class FavoritePlaceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_place);

        final List<String> places = new ArrayList<String>();

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();;
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        FirebaseDatabase.getInstance().getReference("favPlaces/" + currentFirebaseUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            String placename= snapshot.child("placeName").getValue().toString();
                            String placeid = snapshot.child("placeID").getValue().toString();
                            places.add(placename+"\n"+placeid);
                        }
                        function(places);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }

    private void function(List<String> places) {
            String placesList[] = places.toArray(new String[0]);
            ListView favPlacesList = (ListView) findViewById(R.id.favListView);
            FavPlacesAdapter favPlacesAdapter = new FavPlacesAdapter(this, placesList);
            favPlacesList.setAdapter(favPlacesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent_profile = new Intent(FavoritePlaceActivity.this, UserProfileActivity.class);
                startActivity(intent_profile);
                return true;
            case R.id.favPlaces:
                Intent intent_fav = new Intent(FavoritePlaceActivity.this, FavoritePlaceActivity.class);
                startActivity(intent_fav);
                return true;
            case R.id.signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(FavoritePlaceActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
