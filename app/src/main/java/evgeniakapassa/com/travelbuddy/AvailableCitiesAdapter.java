package evgeniakapassa.com.travelbuddy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AvailableCitiesAdapter extends BaseAdapter{
    Context context;
    List<AvailableCity> availableCity;

    AvailableCitiesAdapter(Context context, List<AvailableCity> availableCity) {
        this.context = context;
        this.availableCity = availableCity;
    }

    @Override
    public int getCount() {
        return availableCity.size();
    }

    @Override
    public Object getItem(int position) {
        return availableCity.get(position);
    }

    @Override
    public long getItemId(int position) {
        return availableCity.indexOf(getItem(position));
    }

    /* private view holder class */
    private class ViewHolder {
        ImageView city_img;
        TextView city_name;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.city_list_item, null);
            holder = new ViewHolder();

            ArrayList<String> colors = new ArrayList<String>(
                    Arrays.asList("#48c78283", "#48ee85b5", "#488edce6", "#4898473e"));
            String[] colorsArray = new String[colors.size()];
            colorsArray = colors.toArray(colorsArray);

            holder.city_name = (TextView) convertView
                    .findViewById(R.id.citieslistview_item_city_name);
            holder.city_img = (ImageView) convertView.findViewById(R.id.citieslistview_item_city_img);

            AvailableCity row_pos = availableCity.get(position);

            holder.city_img.setImageResource(row_pos.getCity_img_id());
            holder.city_name.setText(row_pos.getCity_name());

            holder.city_img.setColorFilter(Color.parseColor(colorsArray[position])); // White Tint

            convertView.setTag(holder);

            //onClickListener gia kathe ksexwristo imageButton - city
            ImageButton button =
                    (ImageButton)convertView.findViewById( R.id.citieslistview_item_city_img);
            final TextView itmName = (TextView) convertView.findViewById(R.id.citieslistview_item_city_name);
            button.setOnClickListener(
                    new View.OnClickListener(){
                        public void onClick(View v) {
                            String cityName = (String) itmName.getText();
                            double lat;
                            double lng;
                            switch(cityName) {
                                case "ATHENS":
                                    lat = 37.983810;
                                    lng = 23.727539;
                                    break;
                                case "BARCELONA":
                                    lat = 41.390205;
                                    lng = 2.154007;
                                    break;
                                case "PARIS":
                                    lat = 48.864716;
                                    lng = 2.349014;
                                    break;
                                case "BUDAPEST":
                                    lat = 47.497913;
                                    lng = 19.040236;
                                    break;
                                default:
                                    lat = -33.865143;
                                    lng = 151.209900;
                            }
                            callTheMap(lat, lng);

                        }
                    });

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    private void callTheMap(double lat, double lng) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra("lat",lat);
        intent.putExtra("lng",lng);
        context.startActivity(intent);
    }


}
