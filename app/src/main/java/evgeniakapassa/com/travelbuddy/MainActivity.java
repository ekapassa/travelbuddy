package evgeniakapassa.com.travelbuddy;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static android.widget.Toast.*;

public class MainActivity extends AppCompatActivity{
    String[] city_names;
    TypedArray city_img;

    List<AvailableCity> availableCities;
    ListView availableCities_listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        availableCities = new ArrayList<AvailableCity>();

        city_names = getResources().getStringArray(R.array.cities_names);
        Log.d("NAMES", city_names.toString());
        city_img = getResources().obtainTypedArray(R.array.cities_img);

        for (int i = 0; i < city_names.length; i++) {
            AvailableCity item = new AvailableCity(city_names[i],
                    city_img.getResourceId(i, -1));
            availableCities.add(item);
        }

        availableCities_listview = (ListView) findViewById(R.id.list_available_cities);
        AvailableCitiesAdapter adapter = new AvailableCitiesAdapter(this, availableCities);
        availableCities_listview.setAdapter(adapter);

        //onClickListener gia kathe ksexwristo imageButton - city
        ImageButton button =
                (ImageButton) findViewById( R.id.citieslistview_item_city_img);
        final TextView itmName = (TextView) findViewById(R.id.citieslistview_item_city_name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent_profile = new Intent(MainActivity.this, UserProfileActivity.class);
                startActivity(intent_profile);
                return true;
            case R.id.favPlaces:
                Intent intent_fav = new Intent(MainActivity.this, FavoritePlaceActivity.class);
                startActivity(intent_fav);
                return true;
            case R.id.signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
