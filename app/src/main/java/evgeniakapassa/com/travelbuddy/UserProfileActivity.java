package evgeniakapassa.com.travelbuddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import evgeniakapassa.com.travelbuddy.POJO.User;

public class UserProfileActivity extends AppCompatActivity {

    private TextView name;
    private TextView email;
    private TextView address;
    private TextView gender;
    private TextView birthday;
    //private TextView pass;
    private TextView phone;

    DatabaseReference mDatabase;
    DatabaseReference mUserReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email_data);
        address = (TextView) findViewById(R.id.address_data);
        gender = (TextView) findViewById(R.id.gender_data);
        birthday = (TextView) findViewById(R.id.birthday_data);
        phone = (TextView) findViewById(R.id.phone_data);

        final ImageView profileImg = (ImageView) findViewById(R.id.profile);

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mUserReference = FirebaseDatabase.getInstance().getReference("users/"+currentFirebaseUser.getUid());
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        ValueEventListener messageListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    name.setText(user.name);
                    email.setText(user.email);
                    address.setText(user.address);
                    gender.setText(user.gender);
                    birthday.setText(user.birthday);
                    phone.setText(user.phone);


                    if(user.gender.equals( "Female" )  ){
                        profileImg.setImageResource(R.drawable.girl);
                    }
                    else if(user.gender.equals( "Male" )){
                        profileImg.setImageResource(R.drawable.boy);
                    }
                    else{
                        profileImg.setImageResource(R.drawable.avatar);
                    }

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Failed to read value
            }
        };
        mUserReference.addValueEventListener(messageListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent_profile = new Intent(UserProfileActivity.this, UserProfileActivity.class);
                startActivity(intent_profile);
                return true;
            case R.id.favPlaces:
                Intent intent_fav = new Intent(UserProfileActivity.this, FavoritePlaceActivity.class);
                startActivity(intent_fav);
                return true;
            case R.id.signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(UserProfileActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
