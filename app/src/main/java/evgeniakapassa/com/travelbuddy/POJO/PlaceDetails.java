package evgeniakapassa.com.travelbuddy.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlaceDetails {

    @SerializedName("html_attributions")
    @Expose
    private List<Object> htmlAttributions = new ArrayList<Object>();

    @SerializedName("result")
    @Expose
    private ResultDetails result;

    @SerializedName("status")
    @Expose
    private String status;


    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public ResultDetails getResultDetails() {
        return result;
    }
    public void setResultDetails(ResultDetails result) {
        this.result = result;
    }


}
