package evgeniakapassa.com.travelbuddy.POJO;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Result {

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("place_id")
    @Expose
    private String place_id;

    @SerializedName("name")
    @Expose
    private String name;

//    @SerializedName("opening_hours")
//    @Expose
//    private OpeningHours openingHours;

//    @SerializedName("photos")
//    @Expose
//    private List<Photo> photos = new ArrayList<Photo>();


    @SerializedName("rating")
    @Expose
    private Double rating;

    @SerializedName("price_level")
    @Expose
    private Integer priceLevel;

    @SerializedName("permanently_closed")
    @Expose
    private Boolean permanently_closed;


    public Geometry getGeometry() {
        return geometry;
    }
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPlaceId() {
        return place_id;
    }
    public void setPlaceId(String place_id) {
        this.place_id = place_id;
    }

    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

//    public OpeningHours getOpeningHours() {
//        return openingHours;
//    }
//    public void setOpeningHours(OpeningHours openingHours) {
//        this.openingHours = openingHours;
//    }

    public Double getRating() {
        return rating;
    }
    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getPriceLevel() {
        return priceLevel;
    }
    public void setPriceLevel(Integer priceLevel) {
        this.priceLevel = priceLevel;
    }

    public Boolean getPermClosed() {
        return permanently_closed;
    }
    public void setPermClosed(Boolean permanently_closed) {
        this.permanently_closed = permanently_closed;
    }


}