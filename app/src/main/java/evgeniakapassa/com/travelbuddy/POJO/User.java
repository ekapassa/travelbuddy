package evgeniakapassa.com.travelbuddy.POJO;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String name;
    public String email;
    public String phone;
    public String address;
    public String birthday;
    public String gender;


    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String name, String email, String address, String birthday, String gender, String phone) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.birthday = birthday;
        this.gender = gender;
        this.phone = phone;


    }
}