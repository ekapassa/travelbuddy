package evgeniakapassa.com.travelbuddy.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResultDetails {

    @SerializedName("formatted_address")
    @Expose
    private String formatted_address;

    @SerializedName("international_phone_number")
    @Expose
    private String international_phone_number;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("rating")
    @Expose
    private Float rating;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("vicinity")
    @Expose
    private String vicinity;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("opening_hours")
    @Expose
    private OpeningHours openingHours;



    public String getFormattedAddress() {
        return formatted_address;
    }
    public void setFormattedAddress(String formatted_address) {
        this.formatted_address = formatted_address;
    }


    public String getInternational_phone_numbers() {
        return international_phone_number;
    }
    public void setInternational_phone_number(String international_phone_number) {
        this.international_phone_number = international_phone_number;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Float getRating() {
        return rating;
    }
    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getVicinity() {
        return vicinity;
    }
    public void setVicinity(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }

    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }
    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

}
