package evgeniakapassa.com.travelbuddy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

public class LaunchActivity extends AppCompatActivity {

    private android.widget.Button btnSignup, btnLogin;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LaunchActivity.this, MainActivity.class));
            finish();
        }

        btnLogin = (android.widget.Button) findViewById(R.id.btn_login);
        btnSignup = (android.widget.Button) findViewById(R.id.btn_signup);

    }

    public void SignupBtn(android.view.View view) {

        android.content.Intent intent = new android.content.Intent(LaunchActivity.this, SignupActivity.class);
        startActivity(intent);
        finish();
    }


    public void SigninBtn(android.view.View view) {
        android.content.Intent intent = new android.content.Intent(LaunchActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
