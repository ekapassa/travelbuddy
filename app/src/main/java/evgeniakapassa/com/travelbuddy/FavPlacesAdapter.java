package evgeniakapassa.com.travelbuddy;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.zip.Inflater;

import static android.support.v4.content.ContextCompat.startActivity;

public class FavPlacesAdapter extends BaseAdapter {
    Context context;
    String[] favPlacesList;
    LayoutInflater inflter;



    public FavPlacesAdapter(Context applicationContext, String[] favPlacesList) {
        this.context = context;
        this.favPlacesList = favPlacesList;
        //this.flags = flags;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return favPlacesList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflter.inflate(R.layout.content_fav_list_item, null);
        TextView favPlace = (TextView)view.findViewById(R.id.fav_place_name);
        TextView favPlaceId = (TextView)view.findViewById(R.id.fav_place_id);


        String lines[] = favPlacesList[position].split("\\r?\\n");
        final String placeid = lines[1];
        final String placename = lines[0];

        favPlace.setText(placename);
        favPlaceId.setText(placeid);

        //ImageView icon = (ImageView) view.findViewById(R.id.fav_place_photo);
        //icon.setImageResource(flags[i]);

        //onClickListener gia kathe ksexwristo imageButton - city
        favPlace.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v) {
                        //callDetailsActivity(placename, placeid);
                        Intent intent = new Intent(v.getContext(), PlaceDetailsActivity.class);
                        String array[] = {placeid,placename};
                        intent.putExtra("extras", array);
                        v.getContext().startActivity(intent);

                    }
                });
        return view;
    }

    private void callDetailsActivity(String placename, String placeid) {
        Intent intent = new Intent(context, PlaceDetailsActivity.class);
        String array[] = {placeid,placename};
        intent.putExtra("extras", array);
        context.startActivity(intent);
    }

}