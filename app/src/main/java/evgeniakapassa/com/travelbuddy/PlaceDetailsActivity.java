package evgeniakapassa.com.travelbuddy;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import evgeniakapassa.com.travelbuddy.POJO.Place;
import evgeniakapassa.com.travelbuddy.POJO.PlaceDetails;
import evgeniakapassa.com.travelbuddy.retrofit.PlacesApi;
import evgeniakapassa.com.travelbuddy.utils.G;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class PlaceDetailsActivity extends AppCompatActivity {

    private String placeid;
    private String place_name;
    int PROXIMITY_RADIUS = 5000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_place_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        // getIntent() is a method from the started activity
        Intent intent = getIntent(); // gets the previously created intent
        Bundle extras = getIntent().getExtras();
        String[] extrasArray = extras.getStringArray("extras");
        placeid = extrasArray[0];
        place_name = extrasArray[1];


        get_place_details(placeid, place_name);

        toolbar.setTitle(place_name);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);


        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final String userUuid = currentFirebaseUser.getUid();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        final DatabaseReference mReference = mDatabase.child("favPlaces").child(userUuid).child(placeid);

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    fab.setImageResource(R.drawable.fav_icon);
                } else {
                    fab.setImageResource(R.drawable.fav_icon_filled);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mReference.addListenerForSingleValueEvent(eventListener);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final DatabaseReference mFavPlacesReference = FirebaseDatabase.getInstance().getReference("favPlaces/" + userUuid);
                ValueEventListener eventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (!dataSnapshot.exists()) {
                            fab.setImageResource(R.drawable.fav_icon_filled);
                            Map<String, Object> favMap = new HashMap<>();
                            favMap.put("placeID", placeid);
                            favMap.put("placeName", place_name);
                            mFavPlacesReference.child(placeid).updateChildren(favMap);
                            fab.setImageResource(R.drawable.fav_icon_filled);
                            Snackbar.make(view, "Added to Favorites!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else {
                            fab.setImageResource(R.drawable.fav_icon);
                            mFavPlacesReference.child(placeid).removeValue();
                            Snackbar.make(view, "Removed from Favorites!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                mReference.addListenerForSingleValueEvent(eventListener);
            }
        });

    }

    private void get_place_details(final String placeid, final String place_name) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(G.GOOGLE_PLACES_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PlacesApi service = retrofit.create(PlacesApi.class);
        Call<PlaceDetails> call = service.getPlaceDetails(placeid);
        call.enqueue(new Callback<PlaceDetails>() {
            @Override
            public void onResponse(final Response<PlaceDetails> response, Retrofit retrofit) {
                try {

                    String address = response.body().getResultDetails().getFormattedAddress();
                    TextView place_address = (TextView) findViewById(R.id.address);
                    place_address.setText(address);

                    String phone = response.body().getResultDetails().getInternational_phone_numbers();
                    TextView place_phone = (TextView) findViewById(R.id.phone);
                    place_phone.setText(phone);

                    final String website = response.body().getResultDetails().getWebsite();
                    final TextView place_website = (TextView) findViewById(R.id.website);
                    place_website.setText(website);

                    Float rating = response.body().getResultDetails().getRating();
                    TextView place_rating = (TextView) findViewById(R.id.rating);
                    place_rating.setText(rating.toString());
                    place_rating.setText(rating.toString());


                    List openingHours = response.body().getResultDetails().getOpeningHours().getWeekdayText();
                    //final List<String> openingHoursList = new ArrayList<String>(openingHours);

                    String Monday = (String) openingHours.get(0);
                    String Tue = (String) openingHours.get(1);
                    String Wen = (String) openingHours.get(2);
                    String Thu = (String) openingHours.get(3);
                    String Fr = (String) openingHours.get(4);
                    String Sat = (String) openingHours.get(5);
                    String Sun = (String) openingHours.get(6);

                    TextView Mon = (TextView) findViewById(R.id.Monday);
                    Mon.setText(Monday);
                    TextView Tues = (TextView) findViewById(R.id.Tuesday);
                    Tues.setText(Tue);
                    TextView Wend = (TextView) findViewById(R.id.Wen);
                    Wend.setText(Wen);
                    TextView Thur = (TextView) findViewById(R.id.Thur);
                    Thur.setText(Thu);
                    TextView Fri = (TextView) findViewById(R.id.Fr);
                    Fri.setText(Fr);
                    TextView Satu = (TextView) findViewById(R.id.Sat);
                    Satu.setText(Sat);
                    TextView Sund = (TextView) findViewById(R.id.Sun);
                    Sund.setText(Sun);


                    place_website.setOnClickListener(
                            new View.OnClickListener(){
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    browserIntent.setData(Uri.parse(website));
                                    v.getContext().startActivity(browserIntent);

                                }
                            });



                } catch (Exception e) {

                    Context context = getApplicationContext();
                    CharSequence text = "error on try";
                    int duration = Toast.LENGTH_LONG;

                    Toast toast = Toast.makeText(context, "lalal  " + text, duration);
                    toast.show();

                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }



            }



            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent_profile = new Intent(PlaceDetailsActivity.this, UserProfileActivity.class);
                startActivity(intent_profile);
                return true;
            case R.id.favPlaces:
                Intent intent_fav = new Intent(PlaceDetailsActivity.this, FavoritePlaceActivity.class);
                startActivity(intent_fav);
                return true;
            case R.id.signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(PlaceDetailsActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
