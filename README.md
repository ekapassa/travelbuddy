# TravelBuddy

__An android app for all the people who want to travel to a new city, and explore places. Search the desired ccity based on 
sightseiing, restaurants, bars, shopping etc.__

Android Project for the lesson "Mobile Computing and Applications" of Postgraduate Programme "Digital Systems & Services", 
Department of Digital Systems, University of Piraeus.       

__Developer__: Evgenia Kapassa, evgeniakapassa@gmail.com


## Application Specifications

__App Name__: Travel Buddy
 
__App Description__: "TravelBuddy" is a travel application for exploring points of interest in major cities.
The application allows the user to choose a city from a predefined list, search for restaurants, cafes, banks, pharmacies 
and other places of interest in the city he/she travels to.

__Additional Info__:     
      
* User login / register system
* Select the desired city from a list of "hot" travel destinations
* Select a desired city and redirect to new page
    *  Show map of the city
	*  Select a point of interest type from a list of possible points (eg cafes, restaurants, cinema, etc.)
	*  Show points of interest as markers on the map
* Select a desired point of interest and redirect to a page with a detailed description of the point
* Create user profile
* Create a list of user's favorite points of interest


## Dev Information

### Running the app:
*  Install  the latest Android Studio on your computer - [Install Android Studio](https://developer.android.com/studio/install)
*  Download the project from Bitbucket - [Travel Buddy on Bitbucket](https://bitbucket.org/ekapassa/travelbuddy/src/master/)
*  Import the project to Android Studio
*  Run the app on Android devices or with a simulator

### Components:
* ``LoginActivity , SignupActivity, ResetPasswordActivity`` - User login / register system
* ``MainActivity`` - A ListView of all the available cities
* ``MapsActivity`` - A map for the selected city, where the user can see several points of interest in form of map markers
* ``PlaceDetailsActivity`` - A page with detailed information of the selected point o interest
* ``ProfileActivity`` - A page with the user's information
* ``FavoritePlaceActivity`` - A Listview with the favorite places of the user

### API

Travel Buddy uses two APIs:   
__Place Search - Nearby Search Requests__:  A Nearby Search lets you search for places within a specified area. You can refine your search request 
by supplying keywords or specifying the type of place you are searching for.  More information can be found [here](https://developers.google.com/places/web-service/search)     

__Place Details__:   Once you have a place_id from a Place Search, you can request more details about a particular establishment or point of interest by initiating a Place 
Details request. A Place Details request returns more comprehensive information about the indicated place such as its complete 
address, phone number, user rating and reviews. More information can be found [here](https://developers.google.com/places/web-service/details)


### Data Management for the Application

__Firebase Authentication__:    

Travel Buddy uses Firebase Authetication in order to know the identity of a user. Knowing a user's identity allows the app 
to securely save user data in the cloud and provide the same personalized experience across all of the user's devices.     
__Key capabilities__:  Email and password based authentication     

__Firebase Realtime Database__:    

The Firebase Realtime Database is a cloud-hosted database. Data is stored as JSON and synchronized in realtime to every 
connected client    

Travel buddy uses the `travelbuddy-75a07` database with two collections: `users` and `favPlaces`

**Collection: users**    
    
```
  "users" : {
    "6gREWtz6mUZbf7ukZ9MKdLNTNob2" : {
      "address" : "4993 Depaul Dr.",
      "birthday" : "11/5/1975",
      "email" : "louis.gonzalez24@example.com",
      "gender" : "Male",
      "name" : "Louis Gonzalez",
      "phone" : "(673)-725-9939"
    },
    "po3qUXcO0Qd2AVkIPU0jRqLr6Kw1" : {
      "address" : "6353 Majestic View Ave",
      "birthday" : "1/3/1985",
      "email" : "yolanda.mendoza22@example.com",
      "gender" : "Female",
      "name" : "Yolanda Mendoza",
      "phone" : "(960)-427-9942"
    }
  }
```

**Collection: favPlaces**    
 
``` 
  "favPlaces" : {
    "6gREWtz6mUZbf7ukZ9MKdLNTNob2" : {
      "ChIJkaeipR5u5kcRvaZ7Cew8DQ8" : {
        "placeID" : "ChIJkaeipR5u5kcRvaZ7Cew8DQ8",
        "placeName" : "Starbucks Coffee"
      },
      "ChIJxeyFuBW9oRQRLUlfqit6kBo" : {
        "placeID" : "ChIJxeyFuBW9oRQRLUlfqit6kBo",
        "placeName" : "Aegli Garden"
      }
    },
    "po3qUXcO0Qd2AVkIPU0jRqLr6Kw1" : {
      "ChIJDX8uATC9oRQRRs3qesglgjY" : {
        "placeID" : "ChIJDX8uATC9oRQRRs3qesglgjY",
        "placeName" : "Mike's Irish Bar"
      }
    }
  }
```
